site_name: Cours de Première NSI

theme:
  favicon: assets/images/favicon.png
  icon:
    logo: material/application-variable

  custom_dir: my_theme_customizations/
  name: material
  features:
        - navigation.instant
        - navigation.tabs
        - navigation.expand
        - navigation.top
        - toc.integrate
        - header.autohide

  language: fr
  logo: assets/logo.png
  palette:                        # Palettes de couleurs jour/nuit
      - media: "(prefers-color-scheme: light)"
        scheme: default
        primary: indigo
        accent: indigo
        toggle:
            icon: material/weather-sunny
            name: Passer au mode nuit
      - media: "(prefers-color-scheme: dark)"
        scheme: slate
        primary: blue
        accent: blue
        toggle:
            icon: material/weather-night
            name: Passer au mode jour


markdown_extensions:
  - toc:
      permalink: true

nav:
  - Accueil: index.md
  - Représentation des données: 
    - Présentation: donnees/contenu.md
    - Les opérateurs booléens: donnees/operateurs_booleens.md
    - Les nombres entiers: donnees/repr_entiers.md
    - Les nombres décimaux: donnees/repr_decimaux.md
  - Les données en tables: 
    - Présentation: donnees_en_tables/contenu.md
    - Les tables CSV: donnees_en_tables/csv.md
    - Gestion des fichiers: donnees_en_tables/fichiers.md
    - La bibliothèque csv: donnees_en_tables/csv_library.md
    - La bibliothèque pandas: donnees_en_tables/pandas_library.md

  - Interactions Homme/Machine: 
    - Présentation: ihm/contenu.md
    - HTML et CSS: ihm/html_css.md
    - JavaScript: ihm/js.md
    - Framework Flask: 
      - Présentation et documentation: ihm/flask.md
      - Un TP d'application: ihm/TP_flask.md
  - Architecture et OS: 
    - Présentation: archi_et_os/contenu.md
    - Histoire des ordinateurs: archi_et_os/histoire_ordinateurs.md
    - Les opérateurs booléens: archi_et_os/operateurs_booleens.md
    - Réalisation d'un additionneur: archi_et_os/additionneur.md
    - Fonctionnement des processeurs: archi_et_os/fonctionnement_processeurs.md
    - Architecture d'un ordinateur: archi_et_os/archi_ordinateur.md
    - Les systèmes d'exploitation: archi_et_os/systemes_exploitation.md
    - Les commandes Linux: archi_et_os/commandes_linux.md
    - Les réseaux informatiques: archi_et_os/reseaux.md

  - Langages et programmation: 
    - Présentation: langages/contenu.md
    - Variables et opérations de bases: langages/les_bases.md
    - Constructions élémentaires: langages/les_constructions.md
    - Bien écrire un programme: langages/specifications.md
    - Types de données construites: langages/donnees_construites.md
    - Listes et dictionnaires: langages/dictionnaires.md
  - Algorithmique: 
    - Présentation: algo/contenu.md
    - Présentation des tris: algo/tris.md
    - Invariants de boucles et tris: algo/invariants_tris.md
    - Recherche dichotomique: algo/dichotomie.md
    - Algorithmes gloutons: algo/gloutons.md    
    - Les k plus proches voisins: algo/knn.md
 

markdown_extensions:
    - meta
    - abbr
    - def_list                      # Les listes de définition.
    - attr_list                     # Un peu de CSS et des attributs HTML.
    - footnotes                     # Notes[^1] de bas de page.  [^1]: ma note.
    - admonition                    # Blocs colorés  !!! info "ma remarque"
    - pymdownx.details              #   qui peuvent se plier/déplier.
    - pymdownx.caret                # Passage ^^souligné^^ ou en ^exposant^.
    - pymdownx.mark                 # Passage ==surligné==.
    - pymdownx.tilde                # Passage ~~barré~~ ou en ~indice~.
    - pymdownx.highlight:           # Coloration syntaxique du code
        auto_title: true
        auto_title_map:
            "Python": "🐍 Script Python"
            "Python Console Session": "🐍 Console Python"
            "Text Only": "📋 Texte"
            "E-mail": "📥 Entrée"
            "Text Output": "📤 Sortie"
    - pymdownx.inlinehilite         # pour  `#!python  <python en ligne>`
    - pymdownx.snippets             # Inclusion de fichiers externe.
    - pymdownx.tasklist:            # Cases à cocher  - [ ]  et - [x]
        custom_checkbox:    false   #   avec cases d'origine
        clickable_checkbox: true    #   et cliquables.
    - pymdownx.tabbed               # Volets glissants.  === "Mon volet"
    - pymdownx.superfences          # Imbrication de blocs.
    - pymdownx.keys:                # Touches du clavier.  ++ctrl+d++
        separator: "\uff0b"
    - pymdownx.emoji:               # Émojis  :boom:
        emoji_index:     !!python/name:materialx.emoji.twemoji
        emoji_generator: !!python/name:materialx.emoji.to_svg
    - pymdownx.superfences:
        custom_fences:
          - name: mermaid
            class: mermaid
            format: !!python/name:pymdownx.superfences.fence_code_format
    - pymdownx.tasklist:
        custom_checkbox: true
        clickable_checkbox: true
    - pymdownx.arithmatex:
        generic: true
    - toc:
        permalink: ⚓︎
        toc_depth: 3


extra:
  social:
    - icon: fontawesome/brands/twitter
      link: https://serveur-pgdg.net:8080/
      name: serveur Jupyter
  site_url: https://rougiereric.forge.apps.education.fr/cours-premiere-nsi/
copyright: Copyright &copy; 2019 - 2023 Éric ROUGIER / Paul GODARD


plugins:
  - search
  - macros


extra_javascript:
  - javascripts/config.js
  - https://polyfill.io/v3/polyfill.min.js?features=es6
  - https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js


extra_css:
  - xtra/stylesheets/ajustements.css # ajustements
