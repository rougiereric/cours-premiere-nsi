## 1) Instruction conditionnelle.
!!! info "Définition :"
	Une instruction conditionnelle permet, selon le résultat d'un test booléen, de réaliser certaines instructions 
	si le test est vrai et d'autres s'il est faux.
	
<img alt="Instruction conditionnelle" src="https://pgdg.frama.io/1nsi/langages/img/if_then_else.png" style="width:40%;margin:auto;float:left;"/> 
											
!!! example "Exemple d'instruction conditionnelle :"
	``` python linenums="1"
	nombre = eval(input("Saisir un nombre : "))
	if nombre < 100:
		print(f"{nombre} est inférieur à 100")
	else:
		print(f"{nombre} est supérieur ou égal à 100")
	print("Le test est terminé.")
	```


## 2) Répéter des instructions : les boucles bornées ou boucles `for`.
On utilise la boucle `for` lorsqu’on connaît le nombre de répétitions à effectuer.
!!! info "Définition :"
	Une **boucle bornée** est utilisée lorsque l'on veut répéter un certain nombre de fois les mêmes instructions. On parle d'**itérations**.
	
	Une variable, appelée **variable de boucle**, est utilisée pour compter le **nombre d'itérations**.
	
	En général, cette **variable de boucle** part de `0` et est incrémentée de `1` jusqu'à atteindre la borne supérieure.
	
	Les instructions qui dépendent de la boucle (qui sont répétées) sont **indentées** (décalées à droite) par rapport au **`for`**.




!!! example "Exemples de boucles `for` :"
	``` python linenums="1"
	for compteur in range(3):
		print("Non", end=' ')
		print("et")
	print("... et si !")
	```
	Résultat affiché : 
	> Non et  
	> Non et  
	> Non et  
	> ... et si	  
	
!!! note "Remarque :"
	- `range` signifie "intervalle" en anglais.
	- range(n) renvoie en réalité la liste de tous les entiers de 0 inclu à `n` exclu.
	- Vous pouvez tester cela en tapant list(range(8)).
	- `for i in range(8):` signifie donc littéralement : "pour i prenant successivement les valeurs de 0, 1, 2, 3, 4, 5, 6, 7, faire :"
	- `for i in range(8):` : dans ce cas, `i` est la variable de boucle.

## 3) Répéter des instructions : les boucles non bornées ou boucles `while`.
On utilise la boucle `while` lorsqu’on ne connaît pas le nombre de répétitions à effectuer.
!!! info "Définition :"
	Une **boucle non bornée** est utilisée lorsque l'on veut **répéter** les mêmes instructions **tant qu'une certaine condition est vraie**.
	
	La condition est **vérifiée à chaque passage** dans la boucle. Lorsque cette condition devient fausse, la boucle s'arrête (on sort de la boucle).
	
	Il faut s'assurer que lors de l'exécution des itérations la condition devienne fausse sinon l'algorithme ne s'arrête jamais.

!!! note "Remarque :"

	- La boucle **`while`** est parfois appelée "boucle non bornée" (nombre de boucles n'est pas connu à l'avance) 
	par opposition à la boucle **`for`** qui est appelée "boucle bornée" (le nombre de boucles est fixé à l'avance).
	- Il est possible que la boucle ne soit jamais exécutée, si la condition est initialement fausse.
	- Il est possible que le programme boucle indéfiniment et ne s'arrête jamais, lorsque la condition est toujours vraie. 
	On parle alors de **boucle infinie**.
	- Dans Capytale, il est recommandé de sauvegarder son travail régulièrement car il n'est pas toujours possible 
	d'interrompre le programme en cas de boucle infinie.

Ils effectuent des opérations sur les nombres entiers (integer) ou décimaux (float).

| Opérateur      | Description                          |
| ----------- | ------------------------------------ |
| **`+`**, **`-`**, **`*`** et **`/`**       | Respectivement : l'addition, la soustraction, la multiplication et la division  |
| **`**`**       | Calcule la puissance : `2**4` renvoie `16` |
| **`//`**    | **Division entière** : renvoie le résultat de la division arrondi à l'entier inférieur |
| **`%`**      | **Opérateur modulo** : renvoie le reste de la division entière. |

!!! example "Exemples de boucles `while` :"
	``` python linenums="1"
	temperature = 16
	while temperature < 20 :
		print(f"Température = {temperature}°C")
		temperature = lire_temperature()
	print(f"t = {temperature}°C, fin de chauffe")
	```
	Résultat affiché : 
	> ...  
	> Température = 16°C  
	> Température = 17.2°C  
	> Température = 18.6°C  
	> t = 20.4°C, fin de chauffe  
  

## 4) Les fonctions.
!!! info "Définition :"
	Les **fonctions** permettent de décomposer un algorithme complexe en une série de sous-algorithmes plus simples, 
	lesquels peuvent à leur tour être décomposés en fragments plus petits, et ainsi de suite.
	
	Une fonction exécute une tâche, pour cela on peut lui fournir un ou plusieurs arguments en entrée et 
	elle peut renvoyer une variable en sortie (à l’aide de l’instruction **`return`**).
	
![Exemples de fonction](./img/fonctions.png)
