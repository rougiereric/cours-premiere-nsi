# Utilisation de la bibliothèque `csv`
Source : fiche éduscol sur le [Traitement des données en tables](https://cache.media.eduscol.education.fr/file/NSI/78/1/RA_Lycee_G_NSI_trtd_tables_1170781.pdf) 

La bibliothèque `csv` permet de d'exploiter facilement les données des fichiers `csv`.  

Les exemples ci-dessous utilisent le fichier [countries.csv](./csv/countries.csv).

## Importation des données
Lecture d'un ficher à l'aide de la bibliothèque `csv` :
```python linenums="1"
import csv

pays = []

with open('csv/countries.csv', newline='') as csvfile:
	pays_csv = csv.reader(csvfile, delimiter=';')
	for row in pays_csv:
		pays.append(row)
```

Dans ce cas, les résultats sont stockés sous forme d’un tableau de tableau. On a par exemple :  

- La lecture de la 1ère ligne qui correspond à l'entête  
`>>> pays[0]`  
`:::python ['iso', 'name', 'capital', 'area', 'population', 'continent', 'currency_code', 'currency_name']`   

- La lecture de la 2ème ligne qui correspond au 1er enregistrement.  
`>>> pays[1]`    
`:::python ['AF', 'Afghanistan', '1138958', '647500', '29121286', 'AS', 'AFN', 'Afghani']`  

Ce mode de lecture des données oblige à connaître l'ordre des champs dans les enregistrements.  
On préfère parfois l'organisation en dictionnaire où les clés sont les champs de l'entête: 

```python linenums="1"
import csv
pays = []

with open('csv/countries.csv', newline='') as csvfile:
	pays_csv = csv.DictReader(csvfile, delimiter=';')
	for row in pays_csv:
		pays.append(dict(row))
```

- La lecture du 1er enregistrement s'écrit :  
`>>> pays[0]`  
`:::python {'iso':'AF', 'name':'Afghanistan', 'capital':'1138958', 'area':'647500', 'population':'29121286', 'continent':'AS', 'currency_code':'AFN', 'currency_name':'Afghani'}`  

## Exploitation des données.

### Interrogation des données.
On souhaite retrouver les données correspondant à certains critères. Par exemple, quels sont les pays où l’on paye en euro ?   
```python 
>>> [p['name'] for p in pays if p['currency_code'] == 'EUR']   
 ['Andorra', 'Austria', 'Belgium', ...,  'Spain', 'Vatican']
```

Demandons maintenant quelles sont les monnaies qui s’appellent Dollar. On peut exécuter la ligne suivante :
```python
>>>  [p['currency_code'] for p in pays if p['currency_name'] == 'Dollar']
 ['XCD', 'XCD', 'USD', ..., 'USD', 'ZWL'] 
```
 
On obtient une liste de longueur 54… mais avec beaucoup de répétitions. Pour les supprimer, on peut la transformer en un ensemble
(`:::python set` en anglais). On obtient encore 23 codes de monnaie :
```python
>>> set([p['currency_code'] for p in pays if p['currency_name'] == 'Dollar'])
 {'AUD', 'BBD', ..., 'USD', 'XCD', 'ZWL'}
```  

### Tri des données   
Pour exploiter les données, il peut être intéressant de les trier. Une utilisation possible est l’obtention du classement des entrées
selon tel ou tel critère.  
Une autre utilisation vient du fait que, comme présenté dans la partie algorithmique du programme, la
recherche dichotomique dans un tableau trié est bien plus efficace que la recherche séquentielle dans un tableau quelconque.

Le langage python définit 2 méthodes de tri : `sort` qui trie en place (qui modifie la liste d'origine) et la méthode `sorted` 
qui renvoie une nouvelle liste sans modifier la liste d'origine.  
Il faut définir dans les 2 cas les critères de tris à l'aide de l'argument `key`.  
Par exemple, si l’on veut trier les pays par leur superficie, on doit spécifier la clé `area`. Pour cela, on définit une fonction appropriée :  
```python linenums="1"
def clé_superficie(p):
	return float(p['area'])
```  
Attention, le champ `'area'` est de type `str`, donc si on effectue un tri tel quel, on va obtenir un tri par ordre alphabétique des valeurs de superficie. 
D'où la conversion `:::python float(p['area'])`

Ainsi, pour classer les pays par superficie décroissante, on effectue :  
`:::python >>> pays.sort(key=clé_superficie, reverse=True)`

Par exemple, pour obtenir une liste des cinq plus grands pays (en superficie) :
```python
>>> [(p['name'], p['area']) for p in sorted(pays, key=clé_superficie, reverse=True)[:5]]
 [('Russia', '17100000.0'),
 ('Antarctica', '14000000.0'),
 ('Canada', '9984670.0'),
 ('United States', '9629091.0'),
 ('China', '9596960.0')]
```


### Conclusion
Nous l’avons vu, il est assez facile d’écrire en Python des commandes simples pour exploiter un ensemble de données. Cependant, une utilisation plus poussée 
va vite donner lieu à des programmes fastidieux. Le chapitre suivant présente la bibliothèque `pandas` qui permet une gestion plus efficace de ce genre de traîtement.   
De plus, pour pouvoir exploiter les capitales des pays, nous allons devoir utiliser des données présentes dans un fichier supplémentaire. 
Nous verrons comment le faire facilement à l’aide de la bibliothèque `pandas`.


















