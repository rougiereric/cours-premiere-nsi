# Synthèse du Cours de Première NSI

Ce site regroupe l'ensemble des cours de Première spécialité NSI.
Il est organisé en 6 parties, conformément au référentiel de la spécialité.

- Représentation des données
- Traitements des données en tables
- Interactions entre l'homme et la machine sur le *Web*
- Architectures matérielles et systèmes d'exploitation
- Langages et programmation
- Algorithmique

La page de présentation de chaque partie reprend les attendus du référentiel de la spécialité
 pour la partie concernée.
 
Les activités (TP) évoluant rapidement, elles ne sont pas présentes sur ce site mais 
uniquement sur un serveur [Jupyter](https://serveur-pgdg.net) d'accès reservé aux élèves du lycée Pierre Gilles de Gennes.