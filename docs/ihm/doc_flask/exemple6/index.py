from flask import Flask, render_template, request
from random import randint

app = Flask(__name__)

# Racine
@app.route('/')
def fonction_racine():
    # On recalcule les entiers aléatoire envoyées à la page
    a = randint(1, 100)
    b = randint(1, 100)
    return render_template("index.html", nombre1 = a, nombre2 = b)

@app.route('/somme/')
def fonction_s():
    a = int(request.args['nombre1'])
    b = int(request.args['nombre2'])
    return render_template("somme.html", nombre1 = a, \
        nombre2 = b, somme = a + b)

@app.route('/produit/')
def fonction_p():
    a = int(request.args['nombre1'])
    b = int(request.args['nombre2'])
    return render_template("produit.html", nombre1 = a, \
        nombre2 = b, produit = a * b)

if __name__ == "__main__":
    app.run(debug = True)