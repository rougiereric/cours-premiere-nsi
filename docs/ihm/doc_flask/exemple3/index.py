from flask import Flask, render_template

app = Flask(__name__)

@app.route('/')
def fonction_1():
    # On ajoute des variables et leurs valeurs respectives aux paramètres de la fonction render_template
    return render_template("index.html", texte = "un mot", liste = [7, 2, "mot"], \
        dictionnaire = {"cle1": 12, "cle2": (1, 2)})

if __name__ == "__main__":
    app.run(debug = True)